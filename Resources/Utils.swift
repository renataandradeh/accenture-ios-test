//
//  Utils.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 11/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation
import UIKit

//MARK: - String
extension String {
    func strstr(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        
        if beforeNeedle {
            return String(self[..<range.lowerBound])
        }
        return String(self[range.upperBound...])
    }
    mutating func until(_ string: String) {
        var components = self.components(separatedBy: string)
        self = components[0]
    }
}

//MARK: - Orientation
struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {

        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
}
