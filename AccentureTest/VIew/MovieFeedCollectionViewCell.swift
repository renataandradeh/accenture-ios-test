//
//  MovieFeedCollectionViewCell.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 11/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import UIKit

class MovieFeedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var favoriteButton: FavoriteButton!
    
    var posterFrame: CGRect!
    
    private let connection = Connection()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
    }
    
    var movie: Movie? {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI() {
        guard let movie = movie else { return }
        title.text = movie.title
        
        if let url = URL(string: "https://image.tmdb.org/t/p/w500/\(movie.posterPath)") {
            connection.downloadImage(url: url, posterImageView: posterImageView, posterFrame: posterFrame)
        }
        favoriteButton.updatingFavoriteButtonState(movie: movie)
    }
    @IBAction func favoriteAction(_ sender: Any) {
        guard let movie = movie else { return }
        favoriteButton.isFavorite = favoriteButton.currentImage == favoriteButton.empty ? true : false
        favoriteButton.setFavoriteState(movie: movie)
    }
}
