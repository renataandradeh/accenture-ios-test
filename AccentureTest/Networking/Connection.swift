//
//  Connection.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 11/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation
import UIKit

class Connection {
    //MARK: Download posters
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, posterImageView: UIImageView, posterFrame: CGRect) {
        let activityIndicator = setActivityIndicator(view: posterImageView, viewFrame: posterFrame)
        posterImageView.image = UIImage()
        activityIndicator.startAnimating()
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                posterImageView.image = UIImage(data: data)
                activityIndicator.stopAnimating()
            }
        }
    }
    
    func setActivityIndicator(view: UIView, viewFrame: CGRect) -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.frame.origin = CGPoint(x: viewFrame.size.width/2, y: viewFrame.size.height/2)
        view.addSubview(activityIndicator)
        return activityIndicator
    }
}

extension Connection {
    //  MARK: User Defaults
    func getFavoriteMovies() -> [Movie] {
        if let decodedData = UserDefaults.standard.data(forKey: "favorites") {
            let favoritesMovies = try? JSONDecoder().decode([Movie].self, from: decodedData)
            return favoritesMovies!
        }else{
            return [Movie]()
        }
    }
    
    func saveFavoriteMovie(likedMovie: Movie) {
        var movies = getFavoriteMovies()
        if !(movies.contains(where: { $0.id == likedMovie.id })) {
            movies.append(likedMovie)
            if let encoded = try? JSONEncoder().encode(movies) {
                UserDefaults.standard.set(encoded, forKey: "favorites")
            }
        }
    }
    
    func removeFavoriteMovie(dislikedMovie: Movie) {
        var movies = getFavoriteMovies()
        movies = movies.filter { $0.id != dislikedMovie.id }
        
        if let encoded = try? JSONEncoder().encode(movies) {
            UserDefaults.standard.set(encoded, forKey: "favorites")
        }
    }
}
