//
//  MovieClient.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 10/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation

class MovieClient: APIClient {
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func getFeed(from movieFeedType: MovieFeed, page: Int, completion: @escaping (Result<MovieFeedResult?, APIError>) -> Void) {
        let urlRequest = URLRequest(url: URL(string: "\(movieFeedType.request)&language=en-US&page=\(page)")!)
        fetch(with: urlRequest , decode: { json -> MovieFeedResult? in
            guard let movieFeedResult = json as? MovieFeedResult else { return  nil }
            return movieFeedResult
        }, completion: completion)
    }
    
    func getGenre(from movieFeedType: MovieFeed, completion: @escaping (Result<GenreResult?, APIError>) -> Void) {
        fetch(with: movieFeedType.request , decode: { json -> GenreResult? in
            guard let movieFeedResult = json as? GenreResult else { return  nil }
            return movieFeedResult
        }, completion: completion)
    }
}
