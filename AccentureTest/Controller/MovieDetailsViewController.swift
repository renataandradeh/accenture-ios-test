//
//  MovieDetailsViewController.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 11/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var favoriteButton: FavoriteButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    let empty = UIImage(named: FavoriteIconType.empty.rawValue)
    let full = UIImage(named: FavoriteIconType.full.rawValue)
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    var favoriteMovies: [Movie] = []
    var isFavorite = false
    
    private let connection = Connection()
    private let client = MovieClient()
    
    var movie: Movie?
    
    var genres = [Genre]() {
        didSet {
            updateGenreLabel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
        
        //Fetching genres movies
        client.getGenre(from: .genre) { [weak self] result in
            switch result {
            case .success(let genreResult):
                guard let genreResults = genreResult?.genres else { return }
                self?.genres = genreResults
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        favoriteMovies = connection.getFavoriteMovies()
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        AppUtility.lockOrientation(.all)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        AppUtility.lockOrientation(.all)
    }
    
    func updateUI() {
        
        guard let movie = movie else { return }
        titleLabel.text = movie.title
        yearLabel.text = movie.releaseDate.strstr(needle: "-", beforeNeedle: true)
        overviewLabel.text = movie.overview
        
        if let url = URL(string: "https://image.tmdb.org/t/p/w500/\(movie.posterPath)") {
            connection.downloadImage(url: url, posterImageView: posterImageView, posterFrame: posterImageView.frame)
        }
        favoriteButton.layer.cornerRadius = favoriteButton.frame.size.width/2
        favoriteButton.updatingFavoriteButtonState(movie: movie)
    }
    
    @IBAction func setFavoriteMovie(_ sender: FavoriteButton) {
        guard let movie = movie else { return }
        favoriteButton.isFavorite = favoriteButton.currentImage == favoriteButton.empty ? true : false
        favoriteButton.setFavoriteState(movie: movie)
    }
    
    func updateGenreLabel() {
        guard let movie = movie else { return }
        var genresNames: [String] = []
        for movieGenreId in movie.genreIds {
            for genre in genres {
                if genre.id == movieGenreId {
                    genresNames.append(genre.name)
                }
            }
        }
        for genreName in genresNames {
            if genreName == genresNames.last {
                genreLabel.text = genreLabel.text! + "\(genreName)"
            }else{
                genreLabel.text = genreLabel.text! + "\(genreName), "
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
