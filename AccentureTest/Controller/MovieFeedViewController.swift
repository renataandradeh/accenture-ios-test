//
//  MovieFeedViewController.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 10/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import UIKit

private let reuseIdentifier = "movieCellIdentifier"

class MovieFeedViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedCell: MovieFeedCollectionViewCell!
    
    var filtered: [Movie] = []
    var searchActive : Bool = false
    
    var currentPage = 1
    
    var movies = [Movie]() {
        didSet {
            collectionView?.reloadData()
        }
    }

    private let client = MovieClient()
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Fetching popular movies
        client.getFeed(from: .popular, page: currentPage) { [weak self] result in
            switch result {
            case .success(let movieFeedResult):
                guard let movieResults = movieFeedResult?.results else { return }
                self?.movies = movieResults
                self?.currentPage += 1
            case .failure(let error):
                print("the error \(error)")
            }
        }
        
        // Register cell classes
        self.collectionView?.register(UINib(nibName: "MovieFeedCollectionViewCell", bundle: nil ), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupSearchController()
        collectionView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        
        flowLayout.invalidateLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MovieFeedViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchActive && filtered.count == 0 {
            showSearchEmptyState()
        } else {
            removeEmptyState()
        }
        
        return searchActive ? filtered.count : movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MovieFeedCollectionViewCell
        
        let movie: Movie!
        if searchActive {
            movie = filtered[indexPath.row]
        }else{
            movie = movies[indexPath.row]
        }
        
        cell.posterFrame = cell.frame
        cell.movie = movie
        
        cell.isUserInteractionEnabled = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? MovieFeedCollectionViewCell else { return }
        
        selectedCell = cell
        performSegue(withIdentifier: "movieDetailSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "movieDetailSegue" {
            if let vc = segue.destination as? MovieDetailsViewController {
                vc.movie = selectedCell.movie
            }
        }
    }
    
    //Infinite Scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
        if bottomEdge >= scrollView.contentSize.height{
            //Fetching popular movies
            client.getFeed(from: .popular, page: currentPage) { [weak self] result in
                switch result {
                case .success(let movieFeedResult):
                    guard let movieResults = movieFeedResult?.results else { return }
                    self?.movies.append(contentsOf: movieResults)
                    self?.currentPage += 1
                case .failure(let error):
                    print("the error \(error)")
                }
            }
        }
    }
}

extension MovieFeedViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth: CGFloat = 0.0
        var cellHeight: CGFloat = 0.0
        if UIDevice.current.orientation.isLandscape || UIDevice.current.orientation.isFlat{
            cellWidth = self.view.frame.size.width / 3.5
            cellHeight = self.view.frame.size.height
        } else {
            cellWidth = self.view.frame.size.width / 2.25
            cellHeight = self.view.frame.size.height / 2.25
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let leftRightInset = self.view.frame.size.width / 30.0
        return UIEdgeInsetsMake(0, leftRightInset, 0, leftRightInset)
    }
    
    
}

extension MovieFeedViewController: UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {
    //MARK: - Search Controller
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search movies"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.becomeFirstResponder()
        tabBarController?.navigationItem.titleView = searchController.searchBar
        tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = false
        searchController.searchBar.text = nil
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        let searchString = searchController.searchBar.text
        
        filtered = movies.filter({ (movie : Movie) -> Bool in
            let movieText: NSString = movie.title as NSString
            
            return (movieText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        collectionView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        collectionView.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        collectionView.reloadData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if !searchActive {
            searchActive = true
            collectionView.reloadData()
        }
        
        searchController.searchBar.resignFirstResponder()
    }
}

extension MovieFeedViewController {
    //Empty State
    func showSearchEmptyState() {
        guard let emptyState = Bundle.main.loadNibNamed("SearchEmptyStateView", owner: UIView() , options: nil)?.first as? UIView else { return }
        collectionView.backgroundView = emptyState
        
    }
    func removeEmptyState(){
        collectionView.backgroundView = nil
    }
}
