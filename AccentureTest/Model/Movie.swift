//
//  Movie.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 10/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation

struct Movie: Codable {
    let voteCount: Int
    let id: Int
    let title: String
    let posterPath: String
    let genreIds: [Int]
    let overview: String
    let releaseDate: String
    
    enum CodingKeys: String, Codable, CodingKey {
        case id
        case title
        case overview
        case voteCount = "vote_count"
        case posterPath = "poster_path"
        case genreIds = "genre_ids"
        case releaseDate = "release_date"
    }
}

struct Genre: Codable {
    let id: Int
    let name: String
}
