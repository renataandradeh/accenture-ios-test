//
//  MovieFeed.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 10/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation

enum MovieFeed {
    case popular
    case genre
}
extension MovieFeed: Endpoint {
    
    var base: String {
        return "https://api.themoviedb.org"
    }
    
    var path: String {
        switch self {
        case .popular: return "/3/movie/popular"
        case .genre: return "/3/genre/movie/list"
        }
    }
}
