//
//  MovieFeedResult.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 10/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation

struct MovieFeedResult: Codable {
    let results: [Movie]?
}

struct GenreResult: Codable {
    let genres: [Genre]?
}
