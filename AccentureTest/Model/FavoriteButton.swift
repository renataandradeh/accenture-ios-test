//
//  FavoriteButton.swift
//  AccentureTest
//
//  Created by Renata Gondim Andrade Theóphilo on 11/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import Foundation
import UIKit

enum FavoriteIconType: String {
    case empty = "favorite_empty_icon"
    case full = "favorite_full_icon"
}

class FavoriteButton: UIButton {
    
    let empty = UIImage(named: FavoriteIconType.empty.rawValue)
    let full = UIImage(named: FavoriteIconType.full.rawValue)
    
    var favoriteMovies: [Movie] = []
    var isFavorite = false
    
    private let connection = Connection()
    
    override func awakeFromNib() {
        favoriteMovies = connection.getFavoriteMovies()
    }
    
    func updatingFavoriteButtonState(movie: Movie) {
        favoriteMovies = connection.getFavoriteMovies()
        if favoriteMovies.contains(where: ({$0.title == movie.title})) {
            isFavorite = true
            setFavoriteState(movie: movie)
        } else {
            isFavorite = false
            setFavoriteState(movie: movie)
        }
    }
    
    func setFavoriteState(movie: Movie) {
        if isFavorite {
            setImage(full, for: .normal)
            connection.saveFavoriteMovie(likedMovie: movie)
        }else{
            setImage(empty, for: .normal)
            connection.removeFavoriteMovie(dislikedMovie: movie)
        }
    }
}

