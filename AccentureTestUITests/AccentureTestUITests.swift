//
//  AccentureTestUITests.swift
//  AccentureTestUITests
//
//  Created by Renata Gondim Andrade Theóphilo on 10/04/2018.
//  Copyright © 2018 Renata Gondim Andrade Theóphilo. All rights reserved.
//

import XCTest

class AccentureTestUITests: XCTestCase {
    
    var app: XCUIApplication!
        
    override func setUp() {
        super.setUp()

        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTapMovieCell() {
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 2).children(matching: .other).element.tap()
        
        let backButton = app.navigationBars["AccentureTest.MovieDetailsView"].buttons["Back"]
        backButton.tap()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 0).children(matching: .other).element.tap()
        backButton.tap()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 1).children(matching: .other).element.tap()
        backButton.tap()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 3).children(matching: .other).element.tap()
        backButton.tap()
    }
    func testFavoriteMovie() {
        let app = XCUIApplication()
        let collectionViewsQuery = app.collectionViews
        let cell = collectionViewsQuery.children(matching: .cell).element(boundBy: 0)
        cell.buttons["favorite empty icon"].tap()
        
        let element = collectionViewsQuery.children(matching: .cell).element(boundBy: 1).children(matching: .other).element
        element.tap()
        app.buttons["favorite empty icon"].tap()
        
        let backButton = app.navigationBars["AccentureTest.MovieDetailsView"].buttons["Back"]
        backButton.tap()
        cell.children(matching: .other).element.tap()
        
        let favoriteFullIconButton = app.buttons["favorite full icon"]
        favoriteFullIconButton.tap()
        backButton.tap()
        element.tap()
        favoriteFullIconButton.tap()
        backButton.tap()
    }
    func testEmptyState() {
        app.navigationBars["UITabBar"].searchFields["Search movies"].tap()
        
        let uisearchNavigationBar = app.navigationBars["UISearch"]
        let searchMoviesSearchField = uisearchNavigationBar.searchFields["Search movies"]
        searchMoviesSearchField.typeText("Co")
        
        let deleteKey = app/*@START_MENU_TOKEN@*/.keys["delete"]/*[[".keyboards",".keys[\"Apagar\"]",".keys[\"delete\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        searchMoviesSearchField.typeText("Fif")
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        searchMoviesSearchField.typeText("Qqqq")
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        deleteKey.tap()
        uisearchNavigationBar.tap()
    }
    
    func testScrollCollectionView() {
        let collectionViewsQuery = app.collectionViews
        let element = collectionViewsQuery.children(matching: .cell).element(boundBy: 2).children(matching: .other).element
        element.swipeUp()
        element.swipeUp()
        element.swipeUp()
        element.swipeUp()
        element.swipeUp()
        element.swipeUp()
        element.swipeUp()
        element.swipeUp()
    }
    
    func testOrientationLayout() {
        XCUIDevice.shared.orientation = .landscapeLeft
        XCUIDevice.shared.orientation = .portrait
        
        let app = XCUIApplication()
        app.collectionViews.children(matching: .cell).element(boundBy: 0).children(matching: .other).element.tap()
        XCUIDevice.shared.orientation = .landscapeLeft
        XCUIDevice.shared.orientation = .portrait
        app.navigationBars["AccentureTest.MovieDetailsView"].buttons["Back"].tap()
        XCUIDevice.shared.orientation = .landscapeRight
        XCUIDevice.shared.orientation = .portrait       
    }
}
